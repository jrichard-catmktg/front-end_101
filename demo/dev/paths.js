'use strict';

var js_paths = [
  './dev/js/directives/**/*',
  './dev/js/application.js'
];

module.exports = {
  js: js_paths
}