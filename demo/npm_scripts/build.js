'use strict';

var Gulp = require('gulp');
var Concat = require('gulp-concat');
var Uglify = require('gulp-uglify');
var AssetPaths = require('./../dev/paths.js');

var js_paths = [
  './dev/js/directives/**/*',
  './dev/js/application.js'
];

Gulp.task('build_js', function() {
  Gulp.src(AssetPaths.js)
      .pipe(Concat('prod.js'))
      .pipe(Uglify())
      .pipe(Gulp.dest('./dist/assets/js'))
});

Gulp.start('build_js');